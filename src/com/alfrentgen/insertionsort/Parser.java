package com.alfrentgen.insertionsort;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

interface Parser<T>{
    List<T> parse(List<String> data);
}

class StringParser implements Parser<String>{
    @Override
    public List<String> parse(List<String> data) {
        List<String> parsedData = new ArrayList<String>();
        Pattern p = Pattern.compile("[\\s]");
        Matcher m = p.matcher("");
        for(String s : data){
            m.reset(s);
            if(s.equals("")){
                continue;
            }
            if(m.find()){
                System.out.println("String #: " + "\"" + s + "\"" + " will be omitted due to whitespace occurence.");
            }else{
                parsedData.add(s);
            }
        }
        return parsedData;
    }
}

class IntegerParser implements Parser<Integer>{
    @Override
    public List<Integer> parse(List<String> data) {
        List<Integer> parsedData = new ArrayList<Integer>();
        for(String s : data){
            if(s.equals("")){
                continue;
            }
            int num;
            try {
                num = Integer.parseInt(s);
            }catch(NumberFormatException e){
                System.out.println("String: " + "\"" + s + "\"" + " will be omitted due to wrong format.");
                continue;
            }
            parsedData.add(num);
        }
        return parsedData;
    }
}
