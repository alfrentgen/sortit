package com.alfrentgen.insertionsort;

import java.util.Collections;
import java.util.List;

class Sorter {

    private boolean order;//true - ascending order, false - descending order

    public Sorter(boolean order){
        this.order = order;
    }

    public List<Comparable> sort(List<Comparable> list) {
        for(int cur = 1; cur < list.size(); cur++){
            Comparable element = (Comparable) list.remove(cur);
            int position = cur;
            for(int ndx = cur-1; ndx >= 0; ndx--){
                if(element.compareTo(list.get(ndx)) < 0 ){//element < list.get(ndx)
                    position--;
                }else{
                    break;
                }
            }
            list.add(position, element);
        }
        if(order){
            Collections.reverse(list);
        }
        return list;
    }
}
