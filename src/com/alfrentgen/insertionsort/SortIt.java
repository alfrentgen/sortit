package com.alfrentgen.insertionsort;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.lang.Comparable;

public class SortIt {

    final static String USAGE = "Usage: java -jar InsertionSort.jar inputFile [outputFile] [-s/-i] [-a/-d]\ndefaults: outputfile=out.txt -s -d";
    final static String OUTPUT = "out.txt";

    public static void main(String[] args) {
	// write your code here
        Map<String, String> promptArgs = checkArgs(args);

        File inputFile = new File(promptArgs.get("inputFile"));
        if(promptArgs.get("outputFile") == null){
            System.out.println("No output file name was specified, using \"" + OUTPUT + "\" by default");
            promptArgs.put("outputFile", OUTPUT);
        }
        File outputFile = new File(promptArgs.get("outputFile"));
        checkFiles(inputFile,outputFile);

        List data = readData(inputFile);
        data = parseData(data, promptArgs.get("type"));
        data = sortData(data, promptArgs.get("order"));
        writeData(outputFile, data);
    }

    private static Map<String, String> checkArgs(String[] args){
        List<String> legalOrders = Arrays.asList("-a", "-d");
        List<String> legalTypes = Arrays.asList("-i", "-s");

        //treating first parameter
        if(args.length == 0){
            terminate("At least input file should be presented!\n" + USAGE);
        }
        if(args[0].startsWith("-")){
            terminate("First argument should be a file name\n" + USAGE);
        }

        Map<String, String> argsMap = new TreeMap<String, String>();
        argsMap.put("inputFile", args[0]);
        argsMap.put("order", "d");
        argsMap.put("type", "s");

        //treating second and others parameter
        if(args.length > 1) {
            //second
            if (args[1].startsWith("-")) {
                putArg(args[1], legalOrders.contains(args[1]), legalTypes.contains(args[1]), argsMap);
            } else {
                argsMap.put("outputFile", args[1]);
            }

            //others
            for(int i = 2; i < 4 && i < args.length; i++){
                if (args[i].startsWith("-")) {
                    putArg(args[i], legalOrders.contains(args[i]), legalTypes.contains(args[i]), argsMap);
                }else{
                    terminateOnWrongParameter(args[i]);
                }
            }
        }
        return argsMap;
    }

    private static void putArg(String arg, boolean orderMatched, boolean typeMatched, Map<String, String> argsMap) {
        if (orderMatched){
            argsMap.put("order", arg.substring(1));
        }else if(typeMatched) {
            argsMap.put("type", arg.substring(1));
        }else{
            terminateOnWrongParameter(arg);
        }
    }

    private static void terminateOnWrongParameter(String arg) {
        terminate("Wrong option: " + arg + "\n" + USAGE);
    }

    private static void terminate(String cause) {
        System.out.println(cause);
        System.exit(0);
    }

    private static void checkFiles(File inputFile, File outputFile){
        try {
            if (!inputFile.exists()) {
                throw new IOException("Input file doesn't exist.");
            }
            if (!inputFile.canRead()) {
                throw new IOException("Input file is unavailable for reading.");
            }
            if (inputFile.isDirectory()) {
                throw new IOException("Input file is a directory.");
            }
            if (outputFile.isDirectory()) {
                throw new IOException("Output file is a directory.");
            }
        }catch (IOException e) {
            terminate(e.getMessage());
        }
        try{
            if(!outputFile.exists()) {
                try {
                    outputFile.createNewFile();
                }catch (IOException e){
                    throw new IOException("Cannot create output file.");
                }
            }
            if (!outputFile.canWrite()) {
                throw new IOException("Output file is unavailable for writing.");
            }
        } catch (IOException e) {
            terminate(e.getMessage());
        }
    }

    private static List<String> readData(File inputFile){
        List<String> result = new LinkedList<String>();
        try(BufferedReader br = new BufferedReader(new FileReader(inputFile))) {
            String str;
            while((str = br.readLine())!= null){
                result.add(str);
            }
        } catch (IOException e) {
            terminate(e.getMessage());
        }
        return result;
    }

    private static List parseData(List<String> rawData, String type){
        List parsedData = new LinkedList();
        Parser p;
        if(type.equals("s")){
            p = new StringParser();
        }else{
            p = new IntegerParser();
        }
        return p.parse(rawData);
    }

    private static List sortData(List<Comparable> data, String type){
        boolean order = type.equals("a");
        Sorter s = new Sorter(order);
        return s.sort(data);
    }

    private static void writeData(File outputFile, List outputData){
        try(PrintWriter w = new PrintWriter(new OutputStreamWriter(new FileOutputStream(outputFile), StandardCharsets.UTF_8), true)) {
            for (Object o : outputData) {
                w.println(o.toString());
            }
        } catch (IOException e) {
            System.out.println("Exception occured writing in output file.");
        }
    };
}
